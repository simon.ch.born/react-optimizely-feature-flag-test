import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import OptimizelyHelper from './optimizelyHelper';
import {loadTimeStorageIdent} from './constants.js';

// fetch the datafile as early as possible and store in localStorage
const optimizelyHelper = new OptimizelyHelper();
optimizelyHelper.getAndStoreDatafile();
// get existing user ID or generate randomly and store
optimizelyHelper.getAndStoreUsername();

// simulate a load time of the app
const simulateAppLoadTime = parseInt(localStorage.getItem(loadTimeStorageIdent),10);
window.setTimeout(function(){
  ReactDOM.render(<App />, document.getElementById('root'));
  registerServiceWorker();
},simulateAppLoadTime);
