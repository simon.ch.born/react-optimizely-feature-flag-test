import * as consts from './constants.js';
import optimizely from '@optimizely/optimizely-sdk';
import logger from '@optimizely/optimizely-sdk/lib/plugins/logger';
import enums from '@optimizely/optimizely-sdk/lib/utils/enums';

// TODOS FOR PRODUCTION:
// 1. Remove fakeDatafileDelay (in constructur and function getDatafile)
// 2. Replace getAudienceConditions with real parameters
// 3. Replace "generateRandomUsername" with desired functioning (get already existing anonymous user id)
// 4. Do whatever changes necessary to getAndStoreUsername in order to get the real user id from whatever place
// 5. Nice-to-have: Use addNotificationListeners for Google Analytics integration.
//    If not implemented at first, comment out call to "this.addNotificationListeners" in instantiate function

export default class OptimizelyHelper {
  constructor(){
    this.datafile  = this.getDatafileFromSessionStorage(); // will be null if not stored
    this.instance  = null; // stores the instance
    this.executeAfterInstantiate = []; // array of callbacks after successful instantiating
    // FOR DEMO PURPOSES - fake delay the loading time of the datafile.
    this.fakeDatafileDelay = parseInt(localStorage.getItem(consts.fakeDatafileDelayIdent), 10) || 0;
  }

  createInstance() {
    return optimizely.createInstance({
      datafile: this.datafile,
      logger: logger.createLogger({
        logLevel: enums.LOG_LEVEL.DEBUG,
      }),
    });
  }

  async instantiate() {
    if (!this.datafile){
      this.datafile = await this.getDatafile();
    }
    this.instance = this.createInstance();
    // add listeners for activation and tracking (to be used for Analytics connection)
    this.addNotificationListeners();
    // execute queue after instantiation
    this.executeQueue();
    // make instance, audience and user id available for client-side event triggers
    this.makeGlobal();
  }

  // notification listeners can be used to eg. push data to google analytics
  addNotificationListeners(){
    this.instance.notificationCenter.addNotificationListener(
      enums.NOTIFICATION_TYPES.ACTIVATE,
      function(trackObject){
        console.log("SOMEBODY CALLED ACTIVATE!!!");
        console.log(trackObject);
      }
    );
    this.instance.notificationCenter.addNotificationListener(
      enums.NOTIFICATION_TYPES.TRACK,
      function(trackObject){
        console.log("SOMEBODY CALLED TRACK!!!");
        console.log(trackObject);
      }
    );
  }

  // fetch JSON datafile from CDN
  async getDatafile() {
    if (this.checkIfDatafileStored()){
      return this.getDatafileFromSessionStorage();
    }

    // // FOR DEMO PURPOSES -> artificially delay loading time of datafile
    if (this.fakeDatafileDelay > 0) {
      await new Promise(resolve => setTimeout(resolve,this.fakeDatafileDelay));
    }

    return await fetch(consts.datafileURL)
      .then(function (response) {
        if (response.status >= 400) {
          console.log('Error downloading datafile');
        }
        return response.json();
      });
  }

  async getAndStoreDatafile(){
    this.datafile = await this.getDatafile();
    this.storeDatafile(this.datafile);
  }

  storeDatafile(datafile){
    sessionStorage.setItem(consts.datafileStorageIdent, JSON.stringify(datafile));
  }

  checkIfDatafileStored(){
    const validFileStored = (this.getDatafileFromSessionStorage() !== null && typeof this.getDatafileFromSessionStorage().version !== 'undefined');
    return validFileStored;
  }

  getDatafileFromSessionStorage(){
    return JSON.parse(sessionStorage.getItem(consts.datafileStorageIdent));
  }

  addToQueue(func){
    this.executeAfterInstantiate.push(func);
  }

  executeQueue(){
    while (this.executeAfterInstantiate.length) {
      const func = this.executeAfterInstantiate.pop();
      if (typeof func === 'function'){
        func();
      }
    }
  }

  getAudienceConditions(){
    /***
    /
    / dummy audience function
    / these parameters are handed over when activating an experiment and tracking data
    / ... params in live should be more and better ;)
    /
    /***/
    return {
      'UserAgent': localStorage.getItem('fakeUserAgent') || 'Chrome',
      'Hostname': window.location.hostname,
      'Breakpoint': window.innerWidth >= 768 ? 'small' : 'not_so_small',
      'PageType': window.location.href.indexOf('category') > -1 ? 'category' : 'some_other_page_type',
      'IsFSDCustomer': Math.random() > 0.5 ? 'yes' : 'no',
      'IsUserLoggedIn': Math.random() > 0.5 ? 'yes' : 'no',
      'IsEmployee' : Math.random() > 0.5 ? 'yes' : 'no',
    }
  }

  getAndStoreUsername(){
    const oldName = localStorage.getItem(consts.userIdStorageIdent);
    const newName = oldName || this.generateRandomUsername();
    localStorage.setItem(consts.userIdStorageIdent, newName);
    return newName;
  }

  generateRandomUsername(){
    // dummy. This could generate whatever username
    // or use an existing metro user ID
    return Math.random()+""+Math.random();
  }

  makeGlobal(){
    const optly_handover = {};
    optly_handover.instance = this.instance;
    optly_handover.userId = this.getAndStoreUsername();
    optly_handover.audience = this.getAudienceConditions();
    window.optly_handover = optly_handover;
  }
}
