import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import * as consts from './constants';
import OptimizelyHelper from './optimizelyHelper';

// get an instance of the helper
var optimizelyHelper = new OptimizelyHelper();
optimizelyHelper.instantiate();

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      // variables for test
      // the max timeout for getting a variation can be defined individually in a component
      optly: {
        userId: optimizelyHelper.getAndStoreUsername(), // holds the user ID
        audience: optimizelyHelper.getAudienceConditions(), // holds the user characteristics
        variation: "not_in_test", // will hold the name of the variation the user sees
        maxWait: parseInt(localStorage.getItem(consts.maxComponentWaitIdent),10) || 500, // max wait for component before rendering default
        // ^^ for production this can be a fixed value. the localStorage getter is ONLY FOR DEMO PURPOSES
        timedOut: false,
        resultReceived: false,
      },
      //
      // variables for the app
      //
      titleColor: 'white',
      buttonText: "++- (default) -++",
      showAdvancedStuff: localStorage.getItem(consts.showAdvancedStuffStorageIdent) || 'false',
      showTestOutput:    localStorage.getItem(consts.showTestOutputIdent) || 'false',
      showFeatureOutput: localStorage.getItem(consts.showFeatureOutputIdent) || 'false',
    };

    this.getVariation             = this.getVariation.bind(this);
    this.getButtonTextByVariation = this.getButtonTextByVariation.bind(this);
    this.handleButtonClick        = this.handleButtonClick.bind(this);
    this.overrideUsername         = this.overrideUsername.bind(this);
    this.toggleAdvancedStuff      = this.toggleAdvancedStuff.bind(this);
    this.toggleTestOutput         = this.toggleTestOutput.bind(this);
  }

  componentWillMount() {
    const self = this;

    // try to get variation
    // will be executed without wait if the datafile has already been stored
    if (!this.getVariation()){

      console.log("no optimizely instance available on component will mount");

      // get variation as soon as instance available
      optimizelyHelper.addToQueue(this.getVariation);

      // but set a timeout if it takes too long
      window.setTimeout(function(){
        // check race condition - if no result received in the meantime...
        if (!self.state.optly.resultReceived){
          // ...declare timeout of optimizely
          console.log("optimizely timed out - showing fallback");
          let optly = self.state.optly;
          optly.timedOut = true;
          self.setState({optly});
        }
      },this.state.optly.maxWait);
    }
  }

  getVariation(){

    // only run with valid instance and if not already timed out
    if (optimizelyHelper.instance === null || !optimizelyHelper.instance.isValidInstance || this.state.optly.timedOut) {return false;}

    let optly = this.state.optly;

    // get variation of AB test
    // returns null if user is not in test traffic
    const variation = optimizelyHelper.instance.activate(
      'AB_Test__Create_React_App',
      this.state.optly.userId,
      this.state.optly.audience
    );
    // get data that belongs to this variation (=button decoration and name of variation)
    optly.variation = variation || optly.variation; // stay with default if not in test
    const buttonText = this.getButtonTextByVariation(variation);

    // let render know that it can use the data from the test/feature test
    optly.resultReceived = true;

    // fill state with variation
    this.setState({
      optly,
      buttonText
    });
    return true;
  }

  handleButtonClick(index){
      ///
      // track optimizely event
      //
      optimizelyHelper.instance.track('test_event_2', this.state.optly.userId, this.state.optly.audience);
  }

  render() {

    // if no result from optimizely but still waiting -> show "loading" info
    if (!this.state.optly.timedOut && !this.state.optly.resultReceived) {
      return (<div className="loading">Still waiting for data!</div>);
    }

    const showTestOutputClass = this.state.showTestOutput === 'true' ? 'show-me' : 'hide-me';

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1>React Optimizely Dummy</h1>
        </header>
        <div className="float">
          <p className="explanation">Below you can find the test results for the generated user (<strong>{this.state.optly.userId}</strong>).<br />This user ID is now stored in the localStorage.<br />After any reloads, the user will always see the same variation as before.<br />To get a new variation, press "Clear UserID, reload" or override with your own user ID (eg. your name).</p>
          <h3 onClick={this.toggleTestOutput} className={this.state.showTestOutput === 'true'?'opened':'closed'}>&gt;&gt; Show/Hide Result of A/B Test &lt;&lt;</h3>
          <div className={showTestOutputClass}>
            <p className="App-intro">
              <strong>A/B Test Result:</strong> <br />User <strong>{this.state.optly.userId}</strong> is in <strong>{this.state.optly.variation}</strong>. <br />Button decoration: <strong>{this.state.buttonText}</strong>
              <br />
              <br />
            </p>
          </div>
          <hr />
          <button onClick={()=>this.handleButtonClick()} className="btn-high">{this.state.buttonText} Button {this.state.buttonText}<br />click to fire event (see console)</button>
        </div>
        <div className="float">
          <h3 className="noToggle">Controls</h3>
          <div>
            <input className="large-input" type="text" id="newUser" placeholder="Override UserID"/>
            <br />
            <button className="btn-high" onClick={this.overrideUsername}>Override & store UserID, reload</button>
          </div>
          <br />
          <button className="btn-high" onClick={()=> window.location.reload(true)}>Reload page</button>
          <br />
          <button className="btn-high" onClick={()=> {localStorage.removeItem(consts.userIdStorageIdent); window.location.reload(true);}}>Clear UserID, reload</button>
            <br />
          <button className="btn-high" onClick={()=> sessionStorage.removeItem(consts.datafileStorageIdent)}>Clear stored datafile</button>
            <p className="explanation">Clearing the stored datafile causes it to be requested from Optimizely CDN on app load.</p>
          <hr />
            <p className="explanation">To simulate race conditions, play with the "Advanced stuff" (timings are stored in localStorage, clear manually).</p>
          <h3 onClick={this.toggleAdvancedStuff} className={this.state.showAdvancedStuff === 'true'?'opened':'closed'}>&gt;&gt; Show advanced stuff &lt;&lt;</h3>
          <div className={this.state.showAdvancedStuff === 'true'?'show-me':'hide-me'}>
            <input className="large-input" size="23" type="text" id="fakeLoadTime" placeholder="Set fake load time of app" />
            <br />
            <button className="btn-high" onClick={this.changeFakeLoadTime}>Set and store fake load time</button>
            <br />
            <input className="large-input" size="35" type="text" id="fakeInstantiateDelay" placeholder="Set fake delay for datafile download" />
            <br />
            <button className="btn-high" onClick={this.changeFakeInstantiationDelay}>Set and store fake datafile download delay</button>
            <br />
            <input className="large-input" size="38" type="text" id="maxComponentWait" placeholder="Set max time (ms) for component to wait" />
            <br />
            <button className="btn-high" onClick={this.changeMaxComponentWait}>Set max time for component to wait</button>
          </div>
        </div>
      </div>
    );
  }

  overrideUsername(){
    const optimizelyUserId = document.getElementById('newUser').value;
    localStorage.setItem(consts.userIdStorageIdent, optimizelyUserId);
    window.location.reload(true);
  }

  changeFakeLoadTime(){
    const fakeLoadTimeVal = parseInt(document.getElementById('fakeLoadTime').value,10);
    localStorage.setItem(consts.loadTimeStorageIdent, fakeLoadTimeVal);
    window.location.reload(true);
  }

  changeFakeInstantiationDelay(){
    const fakeInstDelay = parseInt(document.getElementById('fakeInstantiateDelay').value,10);
    localStorage.setItem(consts.fakeDatafileDelayIdent, fakeInstDelay);
    window.location.reload(true);
  }

  changeMaxComponentWait(){
    const fakeInstDelay = parseInt(document.getElementById('maxComponentWait').value,10);
    localStorage.setItem(consts.maxComponentWaitIdent, fakeInstDelay);
    window.location.reload(true);
  }

  toggleAdvancedStuff(){
    const newVal = this.state.showAdvancedStuff === 'true' ? 'false' : 'true';
    localStorage.setItem(consts.showAdvancedStuffStorageIdent, newVal);
    this.setState({showAdvancedStuff: newVal});
  }

  toggleTestOutput(){
    const newVal = this.state.showTestOutput === 'true' ? 'false' : 'true';
    localStorage.setItem(consts.showTestOutputIdent, newVal);
    this.setState({showTestOutput: newVal});
  }

  getButtonTextByVariation(variation){
    let text = '';
    switch (variation){
      case 'variation_1':
        text = '++++++';
        break;
      case 'variation_2':
        text = '||||||';
        break;
      default:
        text = this.state.buttonText;
        break;
    }
    return text;
  }
}

export default App;
