// constants.js
const datafileURL  = 'https://cdn.optimizely.com/datafiles/CoyyKnFiypWqyww3DApJrB.json';
const datafileStorageIdent = 'optimizelyDatafile';
const userIdStorageIdent = 'optimizelyUserId';
const loadTimeStorageIdent = 'optimizelySimulatedLoadTime';
const showAdvancedStuffStorageIdent = 'optimizelyShowAdvancedStuff';
const showTestOutputIdent = 'optimizelyShowTestOutput';
const showFeatureOutputIdent = 'optimizelyShowFeatureOutput';
const fakeDatafileDelayIdent = 'optimizelyFakeDatafileDelay';
const maxComponentWaitIdent = 'optimizelyMaxComponentWait';

module.exports = {
  datafileURL,
  datafileStorageIdent,
  userIdStorageIdent,
  loadTimeStorageIdent,
  showAdvancedStuffStorageIdent,
  fakeDatafileDelayIdent,
  maxComponentWaitIdent,
  showTestOutputIdent,
  showFeatureOutputIdent
}
